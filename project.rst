---
title: Projects
---

- Dr Crow and the Forbidden Zone: A website for a local band, with
  illustrations done in MyPaint, Inkscape and GIMP.
- Obsek Adeventures - A point and click adventure game to contain
  puzzles from Rouse Ball's Mathematical Recreations and Essays and
  other sources.
