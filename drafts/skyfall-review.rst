---
title: Skyfall Review
---


Bond starts with greatness and ends with greatness, lots of nods to old ones and lots of wonderfully shot scenes.

I found it interesting to figure out which action shots were stunt men vs which ones were cgi. I found distasteful that I could tell so well and so quickly. I would imagine that the film did not skimp on the budget it for cgi, so the film probably had the limit at what is possible. At the begginning of the film there is a chase scene across a middle eastern city and fighting on a train. Probably a nod to other bond films. That is where I noticed the swaps between Craig, stunt men and cgi. I think the opening was great and show of how these sort of films are now made. although I noticed the swaps once I had gotten into the story after the opening credits I stopeed thinking about them. More enamoured by the film.

The opening credits were spectacular, I remember seeing the ones for Casino Royale and being really impressed, a proper a presentation of graphical skills. I didn't think these ones were as spectacular due to the seeming mismatch of themes, death, water, smoke. Although they fit for the whole film it didn't seem to fully hang together with so many different colour schemes. Nonetheless it was impressive, and must of cost an extreme amount of rendering time, no matter how big the rig that it was done on.

I don't really intend to review the story, bond films have a certain style to them where the story is glue for the main pieces. An eccentric non-british baddy, stoic bond, quintisentially gentleman genius q, overly arching m, and lots of pretty ladies thrown in for good measure are the charactors, for setting, we had wonderful London, Turkey for a middle Eaastern film, Scotland for a cold setting and the sky scrapers of hong kong. Action sequences including runs through tunnels, across moor, across cities in cars. all put together with amazing cinematography. When the baddy is intorduced he walks in at the end of a long room with bond in the foregroud. An incredibly long time of him walking in and talking was a brilliant scene, I would imagine a few takes to make sure it was done perfectly. the swap between morely and m when they were talking to bond by changing focus rather than position was awe inspiring.

That's it really, it was awesome, a wonderful bond romp round the world with major nods to previous films and great big reveals. Go goddam see it!
