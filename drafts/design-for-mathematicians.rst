---
title: Design for Mathematicians
---


Mathematics and design usually cross over at only two points, statistics for measuring effects and geometry for you know, the looks. However there is an often overlooked area, presentation of technical information. Every mathematician has to do it and the standard tool is LaTeX.

LaTeX is essentially an immensely powerful markup language, with styles pretty much baked in. Just as with every other tool there are fanatics, haters and the average user. The average user probably never goes beyond the standard layouts of article, report and book. The haters couldn't stand a non WYSIWYG editor and probably went back to Word or Google docs. The fanatic writes his own style files, uses the memoir class and has every single part of the document customized. He even sets the margin between his name and the title in the header.

There is another way, use a style file written by a designer. I suggest you look no further than the Edward Tufte class - tufte-latex, once set up your article can look clear and gorgeous every time. Designed around Tufte's famous Visual Display of Quantitative information, it's a perfect way to show off all those pretty graphs you made. Let the designer do the hard work for the mathematician.

A new development combining the power of web applications and LaTeX is work done by ipython and Sage mathematics. Inspired by Mathematica's notebooks they have created their own versions with LaTeX support, ipython's notebook and Sage's use MathJax to display mathematics in it's full glory. The default designs have a bit to be desired in my opinion, however since they are basically just HTML with javascript it shouldn't be hard to sytle them yourself. Especially since ipython's notebook already has Blogger support.

It's an exciting time in displaying and working with data. I'm sure they'll be many more advances and new standards to increase accessibility to the work of Mathematicians and Scientists alike.
