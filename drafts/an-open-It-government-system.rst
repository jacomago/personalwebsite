---
title: An Open IT Government System
---


Wouldn't it be great after staying out late, you could find out all your health infooooooo. Wouldn't it be just fine if I could find out the status of my MP's backed bills. Oh, wouldn't it be wonderful if a government was duplicable!

I have a dream, a dream where all government department IT systems work together and can be used by any government irrespective of the laws they hold. Where control for a citizens data is in the hands of the citizen as long as the law allows it. Where any statistic can be calculated and curated completely anonymously and separately from the data in use.

Proposal

The design of a system for the organization of data and people in a government across all of it's departments. Using the creations of standards for describing data which is separated from the actual implementation.

Basic Idea

Each government system should use the same backend for the storage and management of data, so as any new department can easily be given a new system be replicating another. NO PROPRIARTERY APPLICATIONS OR PRODUCTS SHOULD BE USED. If used they would create a large overhead of having to require to hire people or companies specialized in the particular system for maintenance and upgrades. Using open source/standards such as HTML, IP, TCP, keeps the availability of maintainers high.

Proposal

XML EVERYTHING.

XML is a well card for and well known standard, using it for all documents and input/ output of data has many benefits.

    Easily parseable by many programs.
    Already available on most systems.

The database choice isn't important, you could use several. As long as data can be communicated between databases easily and efficiently. In other words just send XML to each other. If sending XML out only it should easily be possible to write the front end such that getting the data from several databases at once is a doddle. For instance: A judge wishes to view everything on a the prosecuted that is available legally by only searching through one application. Even if all that data resides in several different databases for security/legal reasons. INTEROPERABILITY IS KEY.

Once a system is described with a standard, competing products can be created by third parties if necessary without sacrificing on interoperability. A hospital should be able to buy any hardware it feels is the best deal in it's area and hire developers/companies to set up an IT system which interacts with the local and national ones without any hassle. Not only that, but it should be able to hire Designers not programmers for the respective hospital attendants. 

An example: A new hospital is built, it needs an IT system set up for administrative and data storage needs which interacts with both the local and national governments. A single junior IT professional should be able to set up a working system from a 10 minute tutorial, which can scale with an increase of customers. Presumably this would be a very basic system with a simple template, rather like working on the web with only CSS1. (An example where this would be most beneficiary would be a camp hospital setup to support a natural disaster or transport accident.) The hospital can then hire a local designer or developer to create a theme for the system to make it more pleasing to the eye and easier to be used. (A feel that design can be very much a cultural thing compared to back end development.)

How should this be created

Coimmunication with a standards agency such as IEEE to choose a suitable Project lead. Hire freelance or companies and in house people to work on the project.

The ultimate goal is that given rudimentary computer development knowledge someone can replicate the system on any device they wish to maintain their own government.
Security

The biggest issue. Security should not be compromised at any point, all data should be stored encrypted with only legally able people having the ability to unencrypt. Government security experts should work closely with any third party implementation of the system to be sure that no one makes any compromises. A programmer should be able to write code for the system but not access it. 
