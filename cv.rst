---
title: CV
---

Work Experience
-----------------


4th February 2013 - 13th September 2013
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Software Developer**, `Airedale International Air Conditioning <#www.airedale.com>`_, UK

I used a variaty of technologies in the area of Building Management
Systems. I worked on several BMS Systems using the Niagara Framework 
including one for Pirelli and a data centre setup by Sudlows in the
University of Portsmouth, these were very successful and used as
templates for later work. 

I also worked on Tridium Software Development where I created 3D
Charts, Dials, a Psychrometric chart and other graphical widgets for
use in BMSs. Along with a variety of data manipulation components for
doing linear interpolation, email address management and others.

I comanaged the CentOS testing server used to host a redmine instance,
git repositories, a wiki, a static website and testing version on
Niagara workplace. 


1st December 2012
^^^^^^^^^^^^^^^^^^^^^

**Assistant**, `Pool Creative <#www.pool-creative.com>`_, UK

I worked on a pop up art exhibition for the launch of the new company
Pool Creative, an art selling business for the Yorkshire area. I created
posters and other advertising for the event. I was the photographer and
on hand sound technician.

October 2012 - Febuary 2013
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Web Developer**, Freelance, UK

I created a bespoke website for a local band at
`drcrow <#www.drcow.co.uk>`_, using Django, Heroku and other web
technologies. I created extensive illustrations and designs of a fantasy
world for the band members, using MyPaint, GIMP and inkscape.

Summer 2011
^^^^^^^^^^^^^^^

**Project Researcher**, Nuffield Foundation, UK

I worked through vague research briefs to find and write reports on
Mathematical texts, increasing my written communication and reading
comprehension skills. I created original research in the area of Möbius
transformations and was subsequently published in a respected scientific
journal. I used creative thinking and strong organizational skill to
manage a heavy workload of research and learning of a new skill (LaTeX).

2006 - 09
^^^^^^^^^^^^^

**Designer**, Salsa Smiths, UK

I co-produced albums with Ableton Live and Propellorhead Reason for use
as backing dance tracks and sweet sweet music. They were given out as
advertisements to hundreds of people which tripled dancers compared to
regular events. I also designed the album art and other marketing
material with Adobe Photoshop, this included dressing up as a spider for
a promotional DVD.

June 2005
^^^^^^^^^^^^^

**Sound Engineer Assistant**, Snow Sound Projects, UK

I was involved in the sound at a popular venue in Leeds working with
some big names of the industry including Chip Taylor, the Specials and
others. I worked in a team to set up complicated equipment, liaise with
clients in an informal but work minded atmosphere. I had unforgettable
experiences listening to amateurs and professionals jam on stage for
encores, stadium bands bring huge amounts of kit in tiny vans and
staying up all night to provide equipment for raves.

Education
-----------

2012 July - November
^^^^^^^^^^^^^^^^^^^^^^^^

Institution: **Coursera.org, edX.org**

-  Algorithms Part I
-  Cryptography Part I
-  AI Part I.

2008 - 2012
^^^^^^^^^^^^^^^

Institution: **University of Leeds - UK**

**1st BSc - MMath, Mathematics**

- Dissertation: **Möbius transformations and their Invariants**
- 4th year:    Advanced Computability 92, Hamiltonian Systems 79.
- 3rd year:    Hilbert Spaces and Fourier Analysis 83, Advanced Logic 83.
- 2nd year:    Linear Differential Equations 97, Mathematical Logic 1 83.
- 1st year:    Introductory Group Theory 86, Calculus 86.

2007 - 10
^^^^^^^^^^^^^
Institution: **Derek Fatchet Centre**

CCNA Cisco Certified Network Associate 4.1 Semester 1+2

2006 - 08
^^^^^^^^^^^^^

Institution: **Lawnswood Sixth Form**

- A levels: Mathematics A, Physics B, Chemistry B.
- AS Levels: Further Mathematics A, English Literature C.
- ECDL, Cisco IT Essentials 3.1.

Technical Skills
------------------

Languages & Libraries
^^^^^^^^^^^^^^^^^^^^^^^^^
- Proficient: Python, Django, HTML, CSS,
- Learning: Java, Junit,

Authoring
^^^^^^^^^^^^^
- Text: Org-mode, LaTeX, Emacs
- Graphics: GIMP, Inkscape, Adobe Photoshop
- Audio: Propellorhead Reason, Ableton Live

Version Control
^^^^^^^^^^^^^^^^^^^
- Git

Publications
--------------

-  Projective Cross-Ratio on Hypercomplex Numbers, Advances in Applied
   Clifford Algebras,
   `DOI:10.1007/s00006-012-0335-7 <#DOI-10.1007-s00006-012-0335-7>`_.

Awards and Grants
^^^^^^^^^^^^^^^^^^^^^

- Nuffield Foundation Undergraduate Bursary (£1500)
- Goldsworthy Prize
- (£50). Bronze Senior Mathematical Challenge, 
- Silver Junior Mathematical Challenge.

Interests and Hobbies
-----------------------

I am well-travelled, having visited Portugal, France, Spain, Italy,
Greece, South Africa and Morocco. I like to keep fit by regularly
cycling, which also saves the environment and me money.

I enjoy keeping
up to date with technology and using it to explore my interests in
Mathematics, Art and Music. For Example: I use programming to explore my
particular Mathematical interests include Analytic Number Theory and
Discrete Mathematics. I use my knowledge of Ableton, Reason and drumming
to produce music involving large percussion sets. I love science and
sci-fi which inspires me to create digital artwork and crazy short
stories.

