---
title: The Moments I Live For
tags: philosophy, feelings, writing
---

Walking home with my highschool sweetheart, playing kissing games and making silly ideas come to life.

Flying down a empty road after cycling up the busiest.

Finishing a proof and dancing round the house.

Having a program work and dancing round the house.

Staring at the horizon as it touches the sea, while I sit on a rock overlooking the beach with the Mediterranean sun spread across me.

Looking back on the day and thinking too myself, I did something useful or important or hard today.

Seeing sights you wish you had a camera for.

Laying down with your lover to sleep, with no intentions for making love.

Chatting with a good friend who makes good conversation.

Looking back on hours with a sense of accomplishment.

Giggling at the stupidity of things going wrong.

Finishing a story that makes you think, daaam that was good.

Finishing a story that makes you think, well I never heard anything like that before.

Getting a present just right in such a way they wouldn't buy it for themselves, but they are completely happy and infatuated with it.

Seeing the look on a childs face when you talk about something they are interested in and you are both passionate about.

Going over the previous days work and thinking, actually that was pretty good.

Having a lesson of laughs and learning.

Listening to people talk about their passion with that gleam in their eye that tells you they adore it.
