---
date: 2012-10-28
title: Learning to Program: Part 1
tags: programming, python, webdev
---

.. raw:: html

   <div dir="ltr" style="text-align: left;">

Learning to program is a long difficult task. I started around 4-5 years
ago and feel I've made a little head way so I thought I'd document what
I've gone through. Maybe someone can find some useful advice in these
posts.

I started to learn in the summer of 2008. While in sixth form I was
friends with all four people who took the computing A level. I was also
studying discrete mathematics for further mathematics AS level, at some
point this fueled me enough to find how to program. I came across Peter
Norvigs famous `Teach Yourself Programming in Ten Years`_ and continued
to research. I took out C++: A Beginner's Guide by Herbert Schildt from
the library (the only none html programming book there), after going
through the first chapter I checked review online. I found scaving
reviews and took it back soon after. I wasn't long till I had read a lot
more about learning to program than programming itself, but I now had a
plan: learn python.

Looking back I found it a rather disadvantageous plan. For example it's
not php or C#, yet my area is inundated with such jobs. Unfortunately no
matter how much programmers pan php, business is slow to change. That's
just how the world is. I love python, I love it's dynamic, I love it's
simple syntax and most of all I love that there are no brackets or
semi-colons, but as it's such a new language it's popularity in business
is small (at least in my area).

Another disadvantage was that my plan was way too broad, just like any
other programming language python is ginormous. You don't try to learn a
programming language, that kind of doesn't make any sense. A programming
language is a tool, you learn how to use it within the scope of a
project. For instance I would learn how to use a saw when making a
table. A better plan would be to have a small project, such as build a
blog or read one book. What I did was read the introduction to every
python book and tutorial I could find, not a good idea.

Thankfully, I was saved. I went to university and found a larger love of
mathematics to tinkering with computers. I did however take a course in
programming. I was forced to follow just one set of documentation to
learn how to program. I was given projects where I had to program on my
own from scratch. I was shown pseudocode and functions. Altogether this
was very helpful for making me learn without being distracted.

The programs I wrote were very simple, but the largest I had ever
tackled. I was forced to learn how to debug, as my programs were
incorrect try after try. I cannot overstate the usefulness of having a
structured course, having only one reference really focused my mind.
Everyone learnes differently and some people would of been much better
with having many references. For my learning style, having projects,
deadlines and only one reference was the best.
Some conclusionary advice:

#. Figure out only one project or two - From what job adverts I've seen
   I would advise you to build one standard application and one
   non-standard application. You should work in web or mobile
   development, however popularity changes quickly so working on native
   code isn't a bad idea either. Examples of standard applications are a
   blog, a notepad application, a bookmarking website, a clone of a
   classic game. Examples of non-standard applications are an original
   game, an orignal way of displaying data, a graphics demo. Coming up
   with an idea is really difficult, maybe ask friends and family what
   they would make.
#. Give yourself a deadline - This is probably the hardest part, but one
   of the most important. Working without a deadline really makes
   procrastination easy, I think codeacademy's gamification of learning
   works quite well. Give yourself points or punishments, for example
   each working test 50 points, 1000 points for finishing and -50 points
   for every day after the deadline. I would suggest using an already
   existing deadline such as christmas, new year, or the end of summer.
   Or some time when you start something new anyway such as school,
   university, or a work project.
#. Find your ultimate reference - I would suggest a book, or an
   extremely long tutorial or the best yet an online university course
   such as udacity or coursera or edX. A great place to find your
   ultimate reference is the the reddit `/r/learnprogramming faq`_. I
   started to find working with one reference is incredibly useful for
   stopping your understanding from being vague. For me searching google
   with the keyword python always brings up the python documentation or
   stackoverflow anyway.

.. raw:: html

   </div>

.. raw:: html

   <div class="blogger-post-footer">

.. figure:: https://blogger.googleusercontent.com/tracker/32177205646028298-4811971485195369477?l=blog.cheatashki.co.uk
   :align: center
   :alt: 

.. raw:: html

   </div>

.. raw:: html

   </p>

.. _Teach Yourself Programming in Ten Years: http://norvig.com/21-days.html
.. _/r/learnprogramming faq: http://www.reddit.com/r/learnprogramming/faq
