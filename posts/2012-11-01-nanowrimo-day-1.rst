---
date: 2012-11-01
title: Nanowrimo Day 1
tags: writing, nanowrimo
---

.. raw:: html

   <div dir="ltr" style="text-align: left;">

I've decided to do `backward Nanowrimo`_. I was successful and I wrote
3963 words today. I have an outline for the next around 8000 words.
Hopefully if I plan in advance every couple of days I should make it.
The hardest part was stopping and starting:

I'd just be in the grove when the phone rang. Wrong number.

Takes me a minute or two to realign my brain, and I continue writing.

Dad phones, chat about band website. Writing again.

Right need a drink, get drink.

Writing time. So music on, dam! Playlist ran out of tracks. What do you
mean you have to rehash music database! Try write without music.

Mum comes back, ignore and get music working again.

She asks if still writing, "yes." "Ok I'll leave you."

Soon finish daily allotment, must get more.

One of her friends calls. Arrggghhh, slowing down.

Mum comes downstairs again. Uuuurrgggh stop.

Trails and tribulations.

.. raw:: html

   </div>

.. raw:: html

   <div class="blogger-post-footer">

.. figure:: https://blogger.googleusercontent.com/tracker/32177205646028298-5499503025906938429?l=blog.cheatashki.co.uk
   :align: center
   :alt: 

.. raw:: html

   </div>

.. raw:: html

   </p>

.. _backward Nanowrimo: http://nicolehumphrey.net/backwards-nanowrimo-the-reward-system/
