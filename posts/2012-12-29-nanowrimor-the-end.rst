---
title: Nanowrimo: The End at 40,412 words.
tags: nanowrimo, writing
---

Snarky flarky warky words doddled across my screen,
then out of now where, out of mind words began to gleam.
Oh what horror, oh what subterfuge, how I hate those words,
why, oh why, could they not start from whence they came. Flaunting birds

wrack their bodies across my soul to abolish sense and
ibility. How dare I lose my motivation gland!
Gosh I can't even get to the end of this poem!

Ofcourse not everything needs ot be fully finished, this novel for example! I have continued to plan the second half of my story, My plan was set for 14 chapters which have averaged 7 000 words per chapter. This leaves me with another 57 588 words to write which haven't been planned at all.

I've begun to write out short little parts in my notepad to work with later, I'm a bit fed up with staring at a screen all day. A significant amount of things I quite like to write on my nook and then edit it on the computer. Thumb typing is quite suitable for quick notes, so maybe I will use this method for my novel.

At the same time I've been slowly editing the 40 000 words. It's quite depressing how badly I wrote part of it, I'm very tempted to use it as a template for a complete rewrite. Maybe my nook can be used as a reading/editing device...hmmm I shall have to give it a go.
