---
date: 2012-10-18
title: The Problems of Job Searching
tags: jobs
---

.. raw:: html

   <div dir="ltr" style="text-align: left;">

I've now been searching for a job for around 3 and a half months, it's
getting pretty depressing. I haven't even hit the dreaded 6 months yet.
I've started to find that the self doubt is becoming a really big
problem, being middle class, living at home and having some funds to
play around with... I really don't feel like I even need a job. It all
makes me feel incredibly guilty, there are people out there who would
love to be in my position and are trying their damnedest to get to it.
I am trying to find a job: I'm reading various articles/books on writing
Cv's and Cover letters. I'm keeping at steady rate of job applications
of around 4 a week. I'm continually looking for job outside of what
areas I was before, but more and more I feel like I'm not sure what I'm
qualified for or good at. The only thing I can be sure of is the subject
of my degree: Mathematics.

Mathematics is a lot broader of a subject than most people would think,
it involves a lot of technical skill and also the ability to tell a
story in good English. I've found however that your expressive ability
is fine tuned to a particular audience and a particular method of
writing. When it comes to writing a CV or a Cover Letter, things aren't
so plain and obvious as when writing a proof. The most difference is
there are significantly many more ways of writing them. Oh some people
will say there are lots of proofs for just one theorem, look at
Pythagoras's theorem they'll say, it has hundreds. That is all very
well, but there are only a finite amount of reasonable proofs per
theorem. You may change the wording or structure, but when broken down
to it's essence there are only a finite amount of original proofs for
any one theorem. A CV or a Cover letter can have an infinite array of
descriptions, showing off or dumbing down vocabulary, pointing out stuff
you hated or things you wished for, or just plain sucking up with
extreme compliments.

I must however learn, learn a skill not taught in classrooms or lecture
theaters ( Unless your learning something such as recruitment theory?):
Job Hunting. To tackle Job hunting there are different angles of attack,
they all involve communication they are networking, CV writing and
applications. Good lord have a learned to hate all three. If there is
anything in this world I don't know about well enough, it's talking
about myself. I could bore you for hours on the varied genres of Science
fiction, Mobius transformations or the design of a publication, but
myself! Never! I sit and I think. What have I accomplished? What have I
achieved? What is actually relevant to the job I am applying for? My
mind goes blank. I'm caught in a never ending worry cycle that I haven't
enough achievements for the jobs I'm applying: so I must go do some now.
To I haven't applied for enough jobs: so I must apply for them to gain
more achievements.

I write down in a big list, achievements that I have done no matter what
they are. Increasingly I feel like they are not relevant to the real
world. They are all relevant to fantasy. Music, Art, Mathematics are my
3 staple hobbies and activities, they are voids from fantasy to reality.
How in this reality do I express that they are useful to a would be
employer.

.. raw:: html

   </div>

.. raw:: html

   <div class="blogger-post-footer">

.. figure:: https://blogger.googleusercontent.com/tracker/32177205646028298-5610561058444922207?l=blog.cheatashki.co.uk
   :align: center
   :alt: 

.. raw:: html

   </div>

.. raw:: html

   </p>

