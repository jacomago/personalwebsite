---
title: The Ridicule of Non-Existent Peers
tags: dreams, insomnia, worry
---


Self criticism and my non adversity has become my forte, oh how drol. May I roll around in filth and flay my back till the scorch marks infect my soul. What random chance pray tell does let me forgive myself for it's mischievousness in keeping me awake all night with it's outpouring of guttural thinking.

Flowing thoughts pass by my addled brain as it leaps to far distant lands of fateful self mutilation. Sleep pours out of every seam to be brought into it's murky depths. Yet my brain catches on the edge, determined to not let me fall. Let me bring forth a chalice of perfect gold to conquer you! Let me destroy your conciousness that I may lose sight of my voracious goal.

Oh the ponies and larks and goose filled feathers rack across in hopes of settling my worked out tired brain. May they work in their full boredom and comfort creating subtlety. Yet, here they come, thoughts thrashing the sides, inside and out, of my deepest demons. Scornfully they come and go, adding pure unadulterated ideas and great ideas for the top of mountains. No matter what they are I hate them! I want them out! I want them away! I want peace.

Where oh where lies my tranquillity, when I lay stupendous sights of boring bedroom tease my senses. Eyes open, eyes closed, neither alleviates my strong sense of sharpness. Now is not the time for alertness, yet it keeps on going, keeps on flowing, to the break of dawn.

I wake in a flutter, sadness extends out. Great ideas have been stilled, some lustrous ones keeps alive for the fleeting moments of hiding in the comfort of my quilt house. Soon the pointlessness of staying in place over comes me and I leave to the chilled air of another day.
