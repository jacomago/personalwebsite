---
date: 2012-11-09
title: Dr Crow and his Castle of Plenty
tags: webdev, illustration, GIMP
---

.. raw:: html

   <div dir="ltr" style="text-align: left;">

I have a general plan for the Dr Crow game. At the moment you can
explore some of my very sketchy sketches of rooms and gates. The band
would like a room for each of them, they haven't told me in detail what
kind of rooms, so the sketches are just preliminary.

I sketched out some ideas of an Artist, a gothic Dr, and a Musketeer
then I unfortunately got stuck. Another problem is that all of the
characters are from around the 15 century, not good for a time
travelling castle. The gothic study is OK for Dr Crow, but the others I
might try and come up with particularly instrument themed rooms. A
minstrel for guitar, caveman with big sticks for drums.

I've just finished the `castle`_ drawing, it's a hodge potch of castle
parts from different buildings. Two of the smaller tower tops are from
the Kremlin, the far left hand side is part of a Himeji Castle. The
centre arches are from a basilica in brazil I think and the centre is
from a famous monastery called Angkor Wat.

It was pretty fun to draw, I started out with a very simple layout
sketch which I completely disregarded. I used a large Lonely Planet
photography book to find castles to draw in combination with google
images. Once I had my research material I just sketched each part of the
building in `MyPaint`_. MyPaint is a fairly simple tool, similar to
Corel Painter but with less power. Once I had all my sketches I imported
the file into Gimp and made each sketch a separate layer. Putting all
the sketches together I then went back to MyPaint to add some extra
detail before it was done:

.. raw:: html

   <div class="separator" style="clear: both; text-align: center;">

|image0|

.. raw:: html

   </div>

.. raw:: html

   <div class="separator" style="clear: both; text-align: center;">

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="blogger-post-footer">

.. figure:: https://blogger.googleusercontent.com/tracker/32177205646028298-5319692200288908067?l=blog.cheatashki.co.uk
   :align: center
   :alt: 

.. raw:: html

   </div>

.. raw:: html

   </p>

.. _castle: http://www.drcrow.co.uk/game/dr-crow-and-the-forbidden-zone/3-strip/
.. _MyPaint: http://mypaint.intilinux.com/
.. _|image1|: http://2.bp.blogspot.com/-gOxlYY11w8Q/UJ0KceMmhJI/AAAAAAAACEE/sBtTh9hofM8/s1600/castle-_sized.jpg

.. |image0| image:: http://2.bp.blogspot.com/-gOxlYY11w8Q/UJ0KceMmhJI/AAAAAAAACEE/sBtTh9hofM8/s320/castle-_sized.jpg
.. |image1| image:: http://2.bp.blogspot.com/-gOxlYY11w8Q/UJ0KceMmhJI/AAAAAAAACEE/sBtTh9hofM8/s320/castle-_sized.jpg
