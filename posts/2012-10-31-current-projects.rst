---
date: 2012-10-31
title: Current Projects
tags: webdev, illustration, django, writing, obsek,
---

.. raw:: html

   <div dir="ltr" style="text-align: left;">

I started this blog mainly keep a detailed log of my projects, after all
that's what a blog is, a log. I'm pretty bad at keeping a diary so maybe
this log format will work better for me. So for starters here is a list
of my projects with some summary information:

Obsek adventures
----------------

I invented a character called Fred the obsek when I was younger and
decided to create a point and click adventure game about him. I've also
set out to use it as a basis for creating my first website. To start, I
followed the django tutorial, then used it as basis for creating the
game. I then thought maybe someone has created a webcomic application I
could use as a better starting point, I found `django-comics`_ and built
on that.

Eventually I plan to add a sketch gallery and blog to the website, for
now I mainly need to get on with drawing the pictures. I have a plan of
around 40 frames, but haven't gotten around to painting any in detail. I
want to add the ability for the picture to change rather than the whole
page,  so far I've written this on a static website using jquery.

Dr Crow and the Forbidden Zone
------------------------------

My Dad shared my Obsek website idea with his new band members. They
thought it might be cool to have it on their website, with a house
exploration game. I've started work on the project at `drcrow`_, there
are some sketches in place as beginning ideas. I think the greatest
challenge will be coming up with an original way of incorporating gig
and album news into the site.

Salsa Smiths
------------

My Dad also teaches Salsa. He already has `2`_ `websites`_ but they are
quite lack luster in my opinion. I've started work on a new one based on
the `django-basic-apps`_. I need to add repeating events and create a
new design. I've found I'm being quite a perfectionist about the design
and haven't fully settled on how I think it should look.

`Nanowrimo`_
------------

I intend to try write 50 000 words in one month. I doubt I'll succeed
but I hope to get some good writing experience out of it. More and more
I'm looking for creating rather than consuming. I already have a loose
plan including character personalitys, a main plot and names for
places/people. I intended on writing a tragic love story, but it's
become more of an scifi adventure story. I'm not even sure who is
supposed to fall in love with who at the moment, even though I have all
the characters.

Mathematics: Modular forms and Hyper-complex numbers
----------------------------------------------------

I've been procrastinating most on doing mathematical research. I would
like to study for a Phd in the near future. There are various tasks I
have in this area: I need to rewrite the end of my research paper for
publication because I added some new details, I have some ideas about a
question my supervisor gave me concerning hyperboloids perpendicular ot
the real plane, and I'd like to write either one very long blog post on
my paper or several shorter ones.

Poetry and technology
---------------------

Recently I read William Blake's Songs of Innocence and wrote a
nonShakspearean sonnet. The strong structure of haikus, sonnets and
limericks gave me the idea of trying to stitch tweets together to create
them. I thought maybe using a thesaurus to find tweets on the same
subject, a rhyming dictionary and some way to count syllables you could
stick together different tweets to create poems. They would probably be
mostly nonsense at first, but if you used some kind of voting system you
could get the best ones saved. I would hope there would be several
limericks that are funny just because they don't make sense. If anyone
reads this and would like to make it before me please go ahead, I'd like
to read what happens.

Find a Job and/or PhD.
----------------------

In `"The Problems of Job Searching"`_ I pointed out I'm not sure what I
want to do with myself any more. I enjoy working on these projects, but
I much prefer being given something to work on. I'm just that sort of
person. I need to find out what I'm best suited for, do more networking,
probably join Facebook and figure out what my best achievements are.
Sigh, the project I hate to work on but need to do.

A Few Extras 
-------------

Music
~~~~~

.. raw:: html

   <div style="text-align: left;">

I want to get into music production again, I haven't done any for a
while. I deleted a lot of commercial programs off my computer feeling
like I should try stick to open source or independently made software. I
intend to use either Ardour or Reaper, but as this requires learning a
new program I've put it off a lot. I want to create the sounds from
scratch now, not rely on other peoples samples. To create the samples I
want to use my guitar, my drum sticks and possibly a cajon. I'd also
like to try using a music programming language like `SuperCollider`_ or
some system for python to create samples or even whole tracks.

.. raw:: html

   </div>

Music Hardware
~~~~~~~~~~~~~~

.. raw:: html

   <div style="text-align: left;">

I while ago planned on creating a `monome`_ like application for
android, but now I think it'd be cooler to use the rasberrypi to make a
really strange musical instrument. I'd only use it as a low powered
server otherwise.

.. raw:: html

   </div>

Learning
~~~~~~~~

.. raw:: html

   <div style="text-align: left;">

I've been learning computer science by completing courses on `cousera`_,
`udacity`_ and `edX`_. So far I've completed Algorithms I, Cryptography
I and am now studying AI.

.. raw:: html

   </div>

A last thought
~~~~~~~~~~~~~~

.. raw:: html

   <div style="text-align: left;">

Finally one day I'll use orgmode effectively and daily.

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="blogger-post-footer">

.. figure:: https://blogger.googleusercontent.com/tracker/32177205646028298-2054957477897040512?l=blog.cheatashki.co.uk
   :align: center
   :alt: 

.. raw:: html

   </div>

.. raw:: html

   </p>

.. _django-comics: https://github.com/italomaia/django-comics
.. _drcrow: http://drcrow.herokuapp.com/
.. _2: http://www.salsasmiths.co.uk/
.. _websites: http://www.learnsalsaleeds.co.uk/
.. _django-basic-apps: https://github.com/nathanborror/django-basic-apps
.. _Nanowrimo: http://www.nanowrimo.org/
.. _"The Problems of Job Searching": http://cheatashkisdealings.blogspot.co.uk/2012/10/the-problems-of-job-searching.html
.. _SuperCollider: http://supercollider.sourceforge.net/
.. _monome: http://monome.org/
.. _cousera: http://coursera.org/
.. _udacity: http://udacity.com/
.. _edX: http://edx.org/
