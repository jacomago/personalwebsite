---
date: 2012-10-24
title: Rooted nook simple touch
tags: root, linux, android, nook, apps
---

.. raw:: html

   <div dir="ltr" style="text-align: left;">

.. raw:: html

   <div>

.. raw:: html

   <div dir="ltr" style="text-align: left;">

I've wanted an e-reader for quite a while, and I recently bought a nook
simple touch. I have a netbook which I've been using for reading books
and lots and lots of websites, since I learned that blue light from LCDs
keeps you awake I've felt I should find an alternative solution. E-ink
is a fairly new technology and I've been keeping track of different
e-ink devices since the first Sony reader came out. To keep track
recently, I've been following `the ebook reader`_ as he has very
descriptive reviews with detailed comparisons of most of the ereaders
out there. He also seems to share my passion for tinkering with gadgets
to get the most out of them.

Ever since I learned that the Sony PRS-T1 was an e-ink android tablet in
disguise, I've wanted a device with the capabilities of a computer with
an e-ink screen. After reading various technology websites I found it
was unlikely one was going to ome out soon, hence I bought the nook.
After reading the ebook reader's website I found out that between the
two andoid based eink, the nook has more compatibility with different
apps than the Sony Reader.

.. raw:: html

   </div>

.. raw:: html

   <div dir="ltr" style="text-align: left;">

I found a refurbished nook on ebay, which with customs and delivery cost
around £66, not as great as the original $49 price. Once it arrived I
immediately set about trying out all the possibilities of the device,
then it hit me: "the B&N store" has no free public domain ebooks! It is
the most idiotic thing to leave out, they only would of had to add
`Gutenberg's opds`_ catalog. They could of added to their advertising
spiel, access to thousands of free books.

I therefore went about looking how to root my nook straight away to get
access to as many places for ebooks as possible. I would like to point
out the process is relatively easy, I backed it up following `the ebook
reader`_ and I rooted it following `the ebook reader`_. It took me
around half an hour, with most of the time spent downloading windows
applications to get around the fact I don't know how to use windows cmd
line. Unfortunately even once you've completed this process you can't
download applications through conventional means and have to wait around
24 hours to use the android market. I set about putting ebooks on the
device that I've collected from various sources ( usually hugo award
winning short scifi posted on reddit ). Once I'd done that I could play
around with the default applications left on the device with a much
varied array of files.

I don't have big preferences about how books are displayed, I like my
text relatively small and like seeing where I am in the book thats it.
The default application does the job pretty well, I was comfortable
reading with the 3rd smallest text size and using any of the serif fonts
available. There is a small number showing how many pages read, so I can
keep track easily. However the tinkerer in me can't help try different
applications before settling. There are a number of quality
applications, pretty much all of them have their problems and I ended up
settling on using various different ones.

One of the main things I wanted is the ability to search opds catalogs,
after lamenting for ages that none of the apps worked for that I
discovered mantano. I wasn't very impressed when I first opened it.
Mantano's main bookshelf interface is very heavy and not very pretty on
an eink screen. Although their website says they have designed a full
firmware for eink devices, I found the android application annoying. I
can however search opds catalogs and get access to free books! For the
moment that is all I intend on using it for, I've even replaced the shop
button in the nook quick menu with it. For any books that you can't get
DRM free, I intend to use the kindle application as it's ubiquity and
synchronization is pretty cool.

I have settled on Coolreader for reading ebooks, one of it's great
applications is different tap zones. The screen is split up into 9 parts
and you can customize what happens when you click each zone. As the nook
doesn't have a back button this is really useful for bringing up the
menu, or the table of contents quickly. Other abilities include the
ability to change the full refresh rate of the page, the full refresh
rate of the default nook applications is 6 pages. I find I'm not
bothered by the ghosting shown from partial refresh rate and have set it
up on Coolreader for every 10 pages. Coolreader has a lot of other great
features including custom CSS and choosing your own fonts.

Choosing an ebook reader application is quite boring, once you start
reading you I find you don't really notice how it's displayed. A more
interesting problem was the search for a suitable note taking/sketching
program. The Sony Reader comes with a stylus and some sketching/note
taking application, I wanted to find something similar for the nook as
an alternative to a notepad. Searching google it seemed there wasn't a
great interest in them, most people were happy with evernote and skitch.
I haven't found a perfect answer, but I did eventually find some gems.
The first is a `simple drawing app`_ designed specifically for eink
devices, it only has one brush with two sizes and a simple erase tool. I
used a basic diy stylus made from a cotton bud. a pen and some foil, I
managed to sketch a few figures easily and was very impressed. It's
simplicity is great but I would suggest using the norefresh application
at the same time to make sure it doesn't flash constantly.

I also found `sketch free`_ which was cleverly put into the comics
category. I doubt the pictures advertised on the store were created from
it, but it's also quite nice and simple. It has a large choice of
brushes including web, fur and ribbon which work fairly well on the
screen. I'm not sure if it was my norefresh settings but lines didn't
seem to show up correctly and looked quite faded as if already ghosted,
but it was still quick on the nook.

Going more advanced I would suggest Infinite design which although
slower has such a huge amount of options you can get creations done
quickly. For example it has symmetry tools for drawing the same strokes
reflected through a mirror. Infinite design is a vector based drawing
tool so you can save as an svg or an image file which is very handy for
diagram design.

The hasta la vista of note taking applications with a stylus has to be
freenote. It's ability for handwriting and drawing and typing makes it
goddam awesome. On a phone it doesn't work out so much, with just to
little space to work on. However on the nook I feel like I'm working on
a scifi notepad where what I write is spaced correctly without light
burning into my eyes. Drawings are made as if in a full fledged
sketching application, typing can be put pretty much anywhere. Freenote
is more like a scrapbook than a notebook which is truly awesome. I hope
to take advantage of it's features quickly and actually create stuff
rather than consuming it.

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   </div>

.. raw:: html

   <div class="blogger-post-footer">

.. figure:: https://blogger.googleusercontent.com/tracker/32177205646028298-1524925928499612320?l=blog.cheatashki.co.uk
   :align: center
   :alt: 

.. raw:: html

   </div>

.. raw:: html

   </p>

.. _the ebook reader: http://www.the-ebook-reader.com/
.. _Gutenberg's opds: http://m.gutenberg.org/
.. _the ebook reader: http://blog.the-ebook-reader.com/2012/05/02/how-to-backup-and-restore-nook-glow-and-nook-simple-touch/
.. _the ebook reader: http://www.the-ebook-reader.com/nook-touch-root.html
.. _simple drawing app: http://forum.xda-developers.com/showthread.php?t=1580812
.. _sketch free: https://play.google.com/store/apps/details?id=com.topnet999.android.sketch&hl=en
